﻿# Algorithm Web Application

## Overview
This project utilizes the following technologies:

- .NET Core - API Service
- React - SPA framework
- Docker - Container for solution
- Azure - hosting

Live application is available on [Azure](https://algproject.azurewebsites.net/)

The project has been bootstrapped using VS Studio templates and thus the build targets for both web application and web service are managed by the same project.

## Quickstart
In order to run a local environment, both `NodeJs/NPM` and `.NET Core` have been bundled in a single docker container. To run the setup, change into `CustomerLoyaltyScheme` directory:

* `docker build -t webapp .`
* `docker run -d  -p 80:80 webapp`
