using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using CustomerLoyaltyScheme.Services;
using CustomerLoyaltyScheme.Models;
using CustomerLoyaltyScheme.Data;
using System.Linq;

namespace CustomerLoyaltySchemeTest
{

    [TestClass]
    public class CustomerDataServiceTest
    {
        ICollection<Customer> customers;
        ICustomerDataService customerDataService;
        

        [TestInitialize]
        public void TestInitialize()
        {
            customers = new List<Customer>()
            {
                new Customer("Adam", 500),
                new Customer("Adam", 1500),
                new Customer("Adam", 6200),
                new Customer("David", 200),
                new Customer("Affaf", 2000),
                new Customer("Ramazan", 6200),
                new Customer("Ramazan", 5100),
                new Customer("Saikat", 20),
            };

        }

        [TestMethod]
        public void TestSearchSuccess()
        {
            // instantiate the SearchName class
            customerDataService = new CustomerDataService();

            // Positive test case: invoke the SearchCustomer method on SearchName class and pass the customerList and search string
            ICollection<Customer> SearchList = customerDataService.SearchCustomerName(customers, "ADAM");

            Assert.IsTrue(SearchList.Count == 3);

        }

        [TestMethod]
        public void TestSearchNotFound()
        {
            // instantiate the SearchName class
            customerDataService = new CustomerDataService();

            // Negative test case: invoke the SearchCustomer method on SearchName class and pass the customerList and search string
            ICollection<Customer> SearchList = customerDataService.SearchCustomerName(customers, "Nick");

            Assert.IsTrue(SearchList.Count == 0);

        }


        [TestMethod]
        public Customer TestFindTheHighestLoyaltyPoints()
        {
            // instantiate the SearchName class
            var customerDataService = new CustomerDataService();

            // Total 5 records should be returned: 3 - Adam and 2 - Ramazan
            var customer = customerDataService.FindCustomerWithTheHighestLoyaltyPoints(customers);

            Assert.IsTrue(customer.LoyaltyPoints == 6200);

            return customer;
        }

        [TestMethod]
        public void TestFindDuplicateCustomerWithTheHighestLoyaltyPoints()
        {
            // instantiate the SearchName class
            var customerDataService = new CustomerDataService();

            Customer customer = TestFindTheHighestLoyaltyPoints();

            // Total 5 records should be returned: 3 - Adam and 2 - Ramazan
            var duplicateCustomers = customerDataService.FindCustomerWithDuplicateLoyaltyPoints(customers, customer);

            Assert.IsTrue(duplicateCustomers.Count == 2);
        }

        [TestMethod]
        public void TestSort()
        {
            // instantiate the Sort Driver
            var customerSort = new CustomerSort(customers);
            // obtain sorted list
            var sortedCollection = customerSort.GetList().ToList();
            Assert.IsTrue(sortedCollection.ElementAt(4).FullName == "David");
        }

    }
}
