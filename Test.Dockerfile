﻿FROM microsoft/dotnet:sdk AS build-env
ENV ASPNETCORE_ENVIRONMENT Development
WORKDIR /app

RUN apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash \
    && apt-get install nodejs -yq
    
# Copy everything else and build
COPY . ./

RUN dotnet test