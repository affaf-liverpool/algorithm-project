﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CustomerLoyaltyScheme.Models;

namespace CustomerLoyaltyScheme.Helpers
{
    public static class Helper
    {
        private static Random random = new Random();
        public static IList<Customer> CUSTOMER_LIST = new List<Customer>();

        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }

        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            if (list.Count > 1)
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    T tmp = list[i];
                    int randomIndex = random.Next(i + 1);

                    //Swap elements
                    list[i] = list[randomIndex];
                    list[randomIndex] = tmp;
                }
            }
            return list;
        }
    }
}
