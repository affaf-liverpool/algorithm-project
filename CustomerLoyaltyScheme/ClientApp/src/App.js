import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { Counter } from './components/Counter';
import { CustomerList } from './components/CustomerList';
import { HighestLoyaltyPoint } from './components/HighestLoyaltyPoint';
import { SortCustomerByCategory } from './components/SortCustomerByCategory';
import { DuplicateCustomers } from './components/DuplicateCustomers';
import { SearchCustomer } from './components/SearchCustomer';
import { AverageLoyaltyPoints } from './components/AvarageLoyaltyPoints';

export default class App extends Component {
  displayName = App.name

  render() {
    return (
      <Layout>
		<Route exact path='/' component={Home} />
		<Route path='/displaycustomer' component={CustomerList} />
		<Route path='/displaycustomerswithhighestloyaltypoints' component={HighestLoyaltyPoint} />
        <Route path='/displaycustomerbycategory' component={SortCustomerByCategory} />
        <Route path='/counter' component={Counter} />
		<Route path='/duplicatecustomers' component={DuplicateCustomers} />
		<Route path='/searchcustomer' component={SearchCustomer} />
		<Route path='/displayaverageloyaltypoints' component={AverageLoyaltyPoints} />
      </Layout>
    );
  }
}
