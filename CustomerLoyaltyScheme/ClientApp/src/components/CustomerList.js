import React, { Component } from 'react';

export class CustomerList extends Component {
  displayName = CustomerList.name

  constructor(props) {
    super(props);
    this.state = { customers: [], loading: true };

    fetch('api/Customer/SortCustomerInAlphabeticOrder')
      .then(response => response.json())
      .then(data => {
		  this.setState({ customers: data, loading: false });
      });
  }

	static renderCustomersTable(customers) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>FullName</th>
            <th>LoyaltyPoints</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer =>
            <tr key={customer.id}>
              <td>{customer.fullName}</td>
              <td>{customer.loyaltyPoints}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
		: CustomerList.renderCustomersTable(this.state.customers);

    return (
      <div>
        <h1>The name of its customers in alphabetic order and their points</h1>
        <p>Displays all customers.</p>
        {contents}
      </div>
    );
  }
}
