import React, { Component } from 'react';

export class Home extends Component {
  displayName = Home.name

  render() {
    return (
      <div>
        <h1>Customer Loyalty Application</h1>
        <p>Welcome to Customer Loyalty Application</p>

			<p>To help you get started, please navigate using the navigation.</p>
			<ul>
				<li><strong>Display Customer In Alphabetic Order: </strong> The name of its customers in alphabetic order and their points.</li>
				<li><strong>The Customer(s) With The Highest Loyalty Points: </strong> The name of the customer with the highest loyalty points.</li>
				<li><strong>Display Customers by their Categories </strong> A table displaying all customers by their Membership Category.</li>
				<li><strong>Duplicate Customers </strong>Finding duplicate customer entry.</li>
				<li><strong>Display Average Loyalty Points </strong>Displays the average loyalty points across all customers.</li>
				<li><strong>Search Customer </strong>Searching a customer by his/her name.</li>
			</ul>
      </div>
    );
  }
}
