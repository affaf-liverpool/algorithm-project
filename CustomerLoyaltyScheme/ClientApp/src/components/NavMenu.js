﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Glyphicon, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './NavMenu.css';

export class NavMenu extends Component {
  displayName = NavMenu.name

  render() {
    return (
      <Navbar inverse fixedTop fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={'/'}>Customer Loyalty Scheme</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <LinkContainer to={'/'} exact>
              <NavItem>
                <Glyphicon glyph='home' /> Home
              </NavItem>
            </LinkContainer>
			<LinkContainer to={'/displaycustomer'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> Display Customer In Alphabetic Order
              </NavItem>
            </LinkContainer>
			<LinkContainer to={'/displaycustomerswithhighestloyaltypoints'}>
              <NavItem>
                <Glyphicon glyph='th-list' /> The Customer(s) With The Highest Loyalty Points
              </NavItem>
            </LinkContainer>
            <LinkContainer to={'/displaycustomerbycategory'}>
                <NavItem>
                    <Glyphicon glyph='th-list' /> Display Customers by their Categories
                </NavItem>
            </LinkContainer>
			 <LinkContainer to={'/duplicatecustomers'}>
                <NavItem>
                    <Glyphicon glyph='th-list' /> Duplicate Customers
                </NavItem>
            </LinkContainer>
			<LinkContainer to={'/displayaverageloyaltypoints'}>
                <NavItem>
                    <Glyphicon glyph='th-list' /> Display Average Loyalty Points
                </NavItem>
            </LinkContainer>
			<LinkContainer to={'/searchcustomer'}>
                <NavItem>
                    <Glyphicon glyph='th-list' /> Search Customer
                </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
