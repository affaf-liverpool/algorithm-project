import React, { Component } from 'react';

export class DuplicateCustomers extends Component {
  displayName = DuplicateCustomers.name

  constructor(props) {
    super(props);
    this.state = { customers: [], loading: true };

    fetch('api/Customer/DuplicateCustomers')
      .then(response => response.json())
      .then(data => {
		  this.setState({ customers: data, loading: false });
      });
  }

	static renderCustomersTable(customers) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>FullName</th>
            <th>LoyaltyPoints</th>
			<th>Category</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer =>
            <tr key={customer.id}>
              <td>{customer.fullName}</td>
              <td>{customer.loyaltyPoints}</td>
			  <td>{customer.category}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
		: DuplicateCustomers.renderCustomersTable(this.state.customers);

    return (
      <div>
        <h1>Finding duplicate customer entry </h1>
        <p>(i.e. the same customer is listed twice; one should be able to find such duplicate entries by customer name).</p>
        {contents}
      </div>
    );
  }
}
