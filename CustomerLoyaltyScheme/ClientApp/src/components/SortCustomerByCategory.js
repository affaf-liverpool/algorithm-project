import React, { Component } from 'react';

export class SortCustomerByCategory extends Component {
    displayName = SortCustomerByCategory.name

  constructor(props) {
    super(props);
    this.state = { customers: [], loading: true };

      fetch('api/customer/DisplayCustomersWithCategory')
      .then(response => response.json())
      .then(data => {
		  this.setState({ customers: data, loading: false });
      });
  }

	static renderCustomersTable(customers) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>FullName</th>
            <th>LoyaltyPoints</th>
            <th>Category</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer =>
            <tr key={customer.id}>
              <td>{customer.fullName}</td>
              <td>{customer.loyaltyPoints}</td>
              <td>{customer.category}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
        : SortCustomerByCategory.renderCustomersTable(this.state.customers);

    return (
      <div>
        <h1>A table displaying all customers by their Membership Category</h1>
        {contents}
      </div>
    );
  }
}
