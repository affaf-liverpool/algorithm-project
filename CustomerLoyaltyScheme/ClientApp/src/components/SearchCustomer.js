import React, { Component } from 'react';

export class SearchCustomer extends Component {
	displayName = SearchCustomer.name

	constructor(props) {
		super(props);
		this.state = { customers: [], loading: true, value: '' };

		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);

		fetch('api/Customer/GetCustomers')
			.then(response => response.json())
			.then(data => {
				this.setState({ customers: data, loading: false });
			});
	}

	handleChange(event) {
		this.setState({ value: event.target.value });
	}

	handleSubmit() {

		fetch('api/Customer/SearchCustomer/name=' + this.state.value)
			.then(response => response.json())
			.then(data => {
				this.setState({ customers: data, loading: false, value: this.state.value });
			});

	}

	static renderForecastsTable(customers) {
		return (
			<table className='table'>
				<thead>
					<tr>
						<th>FullName</th>
						<th>LoyaltyPoints</th>
						<th>Category</th>
					</tr>
				</thead>
				<tbody>
					{customers.map(customer =>
						<tr key={customer.id}>
							<td>{customer.fullName}</td>
							<td>{customer.loyaltyPoints}</td>
							<td>{customer.category}</td>
						</tr>
					)}
				</tbody>
			</table>
		);
	}

	render() {

		let contents;
		if (this.state.customers) {
			contents = this.state.loading
				? <p><em>Loading...</em></p>
				: SearchCustomer.renderForecastsTable(this.state.customers);
		}

		return (
			<div>
				<h1>Search Customer</h1>

				<label>
					Name: &emsp; 
						<input type="text" value={this.state.value} onChange={this.handleChange} />
					&emsp;
				</label>
				<button onClick={this.handleSubmit}>Search</button>
				{contents}
			</div>
		);
	}
}
