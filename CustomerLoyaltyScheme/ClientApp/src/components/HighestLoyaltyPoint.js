import React, { Component } from 'react';

export class HighestLoyaltyPoint extends Component {
  displayName = HighestLoyaltyPoint.name

  constructor(props) {
    super(props);
    this.state = { customers: [], loading: true };

    fetch('api/Customer/DisplayCustomersWithHighestLoyaltyPoints')
      .then(response => response.json())
      .then(data => {
		  this.setState({ customers: data, loading: false });
      });
  }

	static renderCustomersTable(customers) {
    return (
      <table className='table'>
        <thead>
          <tr>
            <th>FullName</th>
            <th>LoyaltyPoints</th>
          </tr>
        </thead>
        <tbody>
          {customers.map(customer =>
            <tr key={customer.id}>
              <td>{customer.fullName}</td>
              <td>{customer.loyaltyPoints}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
		: HighestLoyaltyPoint.renderCustomersTable(this.state.customers);

    return (
      <div>
        <h1>The name of the customer with the highest loyalty points.</h1>
        <p>Displays the name of customers with highest loyalty points</p>
        {contents}
      </div>
    );
  }
}
