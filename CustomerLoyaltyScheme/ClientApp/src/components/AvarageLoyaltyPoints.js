import React, { Component } from 'react';

export class AverageLoyaltyPoints extends Component {
	displayName = AverageLoyaltyPoints.name

	constructor(props) {
		super(props);
		this.state = { loyaltyPoints: "", loading: true };

		fetch('api/Customer/AverageLoyaltyPoints')
			.then(response => response.text())
			.then(data => {
				this.setState({ loyaltyPoints: data, loading: false });
			});
	}

	static renderCustomersTable(loyaltyPoints) {
		return (
			<h4> Average Customer Loyalty Points is {loyaltyPoints}</h4>
		);
	}

	render() {
		let contents = this.state.loading
			? <p><em>Loading...</em></p>
			: AverageLoyaltyPoints.renderCustomersTable(this.state.loyaltyPoints);

		return (
			<div>
				<h1>Displays the average loyalty points across all customers.</h1>
				{contents}
			</div>
		);
	}
}
