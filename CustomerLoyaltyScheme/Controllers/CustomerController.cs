﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerLoyaltyScheme.Data;
using CustomerLoyaltyScheme.Models;
using CustomerLoyaltyScheme.Services;
using Microsoft.AspNetCore.Mvc;

namespace CustomerLoyaltyScheme.Controllers
{
    /*
     
        APIs:

        1-) HTTPGET: api/customer/displaycustomers
        response: application/json List<Customer> customers

        2-) HTTPGET: api/customer/displaycustomerswithcategory
        response: application/json List<Customer> customers

        3-) HTTPGET: api/customer/displaycustomerswithhighestloyaltypoints
        response: application/json List<Customer> customers

        4-) HTTPGET: api/customer/avarageloyaltypoint
        response: application/json int avarageLoyalty

        5-) HTTPGET: api/customer/search?text="dd"
        response: application/json List<Customer> customers

    6-) HTTPGET: api/customer/findduplicates
    response: application/json List<Customer> customers

     */
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private ICollection<Customer> _customers;
        private ICustomerDataService _customerDataService;

        public CustomerController(ICustomerDataService customerDataService, IDataSource dataSource)
        {
            _customers = dataSource.GetSeedData();
            _customerDataService = customerDataService;
        }

        [HttpGet("[action]")]
        public IEnumerable<Customer> GetCustomers()
        {;
            return _customers;
        }

        [HttpGet("[action]")]
        public IEnumerable<Customer> DisplayCustomersWithHighestLoyaltyPoints()
        {
            //Get the customer with highest loyalty point
            var customer = _customerDataService.FindCustomerWithTheHighestLoyaltyPoints(_customers);

            //Get if the max loyalty points have duplicates
            var customers = _customerDataService.FindCustomerWithDuplicateLoyaltyPoints(_customers, customer);

            return customers;
        }

        [HttpGet("[action]")]
        public IEnumerable<Customer> DisplayCustomersWithCategory()
        {
            var customers = _customerDataService.SortCustomerByCategory(_customers);
            return customers;
        }

        [HttpGet("[action]")]
        public IEnumerable<Customer> DuplicateCustomers()
        {
            return _customerDataService.FindDuplicateCustomerNames(_customers);
        }

        [HttpGet("[action]/name={name}")]
        public IEnumerable<Customer> SearchCustomer(string name)
        {
            return _customerDataService.SearchCustomerName(_customers, name);
        }

        [HttpGet("[action]")]
        public int AverageLoyaltyPoints()
        {
            //Get the customer with highest loyalty point
            var average = _customerDataService.GetAverageLoyaltyPoints(_customers);
            return average;
        }

        [HttpGet("[action]")]
        public IEnumerable<Customer> SortCustomerInAlphabeticOrder()
        {
            // list is sorted on assignment
            return _customers;
        }



    }
}
