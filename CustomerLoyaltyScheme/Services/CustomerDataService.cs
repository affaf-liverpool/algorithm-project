﻿using CustomerLoyaltyScheme.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomerLoyaltyScheme.Services
{
    public class CustomerDataService:ICustomerDataService
    {
        public CustomerDataService()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="customers"></param>
        /// <returns></returns>
        public Customer FindCustomerWithTheHighestLoyaltyPoints(ICollection<Customer> customers)
        {
            //Get HighestLoyalty Points of The First Item and assign to a max value
            int max = customers.FirstOrDefault().LoyaltyPoints;

            //Iterate all items of list
            foreach (var customer in customers)
            {
                //check if the value is bigger than max, if yes than assign to the max value
                if (customer.LoyaltyPoints > max)
                {
                    max = customer.LoyaltyPoints;
                }
            }
            // return customer that get the max
            return customers.FirstOrDefault(x => x.LoyaltyPoints == max);
        }


        public ICollection<Customer> FindCustomerWithDuplicateLoyaltyPoints(ICollection<Customer> customers, Customer customer)
        {
            //Create a new list to save duplicated customer into it
            ICollection<Customer> duplicateCustomers = new List<Customer>();

            //Iterate all items of list
            foreach (var cus in customers)
            {
                if (cus.LoyaltyPoints == customer.LoyaltyPoints)
                {
                    duplicateCustomers.Add(cus);
                }
            }

            return duplicateCustomers;
        }


        // method to search for all customer records within a list matching the given name 
        public ICollection<Customer> SearchCustomerName(ICollection<Customer> customerList, string name)
        {
            // instantiate an empty list of customer record to hold the search result
            ICollection<Customer> customerSearchCollection = new List<Customer>();

            // loop for each value of the list item parameter
            foreach (var customer in customerList)
            {
                // perform a case insensitive comparision of the customer name to the name param
                if (string.Compare(customer.FullName, name, StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    // if the name matches add the customer record along with the loyalty points to the list to be returned back
                    customerSearchCollection.Add(customer);
                }
            }

            // return back the search list containing customer records
            return customerSearchCollection;
        }



        // method to search for all customer records within a list and return back duplicate customers 
        public ICollection<Customer> FindDuplicateCustomerNames(ICollection<Customer> customerList)
        {
            // instantiate an empty list of customer record to hold the search result
            ICollection<Customer> customerSearchCollection = new List<Customer>();

            // get the list of netsted duplicate customer records ignoring case
            var duplicatedList = customerList.GroupBy(cust => cust.FullName, StringComparer.OrdinalIgnoreCase).Where(g => g.Count() > 1).Select(g => g).ToList();

            // build the collection to be returned back
            foreach (var group in duplicatedList)
            {
                foreach (var customer in group)
                {
                    customerSearchCollection.Add(customer);
                }
            }

            // return back the search list containing customer records
            return customerSearchCollection;
        }


        // An array is used because we need to manipulate by predictable keys, and this is the easiest way to do it i believe.
        private void InternalGroupSorting(Customer[] customers, int pos, Customer customer, Dictionary<string, int> sortOrder)
        {
            int prevPos = pos - 1;
            while (prevPos >= 0 && (sortOrder[customers[prevPos].Category] > sortOrder[customer.Category]))
            {
                customers[prevPos + 1] = customers[prevPos];
                prevPos -= 1;
            }
            customers[prevPos + 1] = customer;

        }

        public ICollection<Customer> SortCustomerByCategory(ICollection<Customer> customers)
        {
            // Define the customisable sort order.
            Dictionary<string, int> sortOrder = new Dictionary<string, int>
            {
                { "Platinum", 1 },
                { "Gold", 2 },
                { "Silver", 3 },
                {"Undefined", 4 }
            };

            // Convert the ICollection into an array
            Customer[] customerArray = customers.ToArray<Customer>();

            // Perform the actual sort.
            for (int i = 1; i < customerArray.Length; i++)
            {
                Customer currentCustomer = customerArray[i];
                InternalGroupSorting(customerArray, i, currentCustomer, sortOrder);
            }

            return new List<Customer>(customerArray);
        }

        public int GetAverageLoyaltyPoints(ICollection<Customer> customers)
        {
            var total = 0;
            foreach (Customer customer in customers)
            {
                total += customer.LoyaltyPoints;
            }
            var average = total / customers.Count;
            return average;
        }

        public ICollection<Customer> SortCustomerInAlphabeticOrder(ICollection<Customer> customers)
        {
            // instantiate the Sort Driver
            var customerSort = new CustomerSort(customers);
            // obtain sorted list
            return customerSort.GetList().ToList();
        }
    }
}
