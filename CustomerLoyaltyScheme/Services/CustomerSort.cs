﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using CustomerLoyaltyScheme.Models;
using CustomerLoyaltyScheme.Helpers;

namespace CustomerLoyaltyScheme.Services
{
    public class CustomerSort
    {
        private List<Customer> sortedList;

        public CustomerSort(ICollection<Customer> customerList)
        {
            // shuffle given list and assign to placeholder list
            sortedList = (List<Customer>)customerList.ToList().Shuffle();
            Sort(sortedList, 0, sortedList.Count - 1, 0);
        }

        /// <summary>
        /// Sorted List getteer
        /// </summary>
        /// <returns>The list.</returns>
        public ICollection<Customer> GetList()
        {
            return new Collection<Customer>(sortedList);
        }

        /// <summary>
        /// Returns the char at given index
        /// </summary>
        /// <returns>The <see cref="T:System.Char"/>.</returns>
        /// <param name="s">S.</param>
        /// <param name="position">position.</param>
        private char CharAt(String s, int position)
        {
            if (position == s.Length) return (char) 0;
            return s[position];
        }

        /// <summary>
        /// Sort the specified list, lowerBound, higherBound and position.
        /// </summary>
        /// <param name="list">List.</param>
        /// <param name="lowerBound">lowerBound.</param>
        /// <param name="higherBound">higherBound.</param>
        /// <param name="position">position.</param>
        private void Sort(IList<Customer> list, int lowerBound, int higherBound, int position)
        {

            int lessThan = lowerBound;
            int greaterThan = higherBound;
            int i = lowerBound + 1;
            int charAtPosition = 0;

            if (lowerBound >= list.Count)
            {   charAtPosition = CharAt(list.ElementAt(list.Count-1).FullName, position);
            } else
            {
                charAtPosition = CharAt(list.ElementAt(lowerBound).FullName, position);
            }


            // insertion sort for optimization
            if (higherBound <= lowerBound + 10)
            {
                insertion(list, lowerBound, higherBound, position);
                return;
            }

            while (i <= greaterThan)
            {
                int charPositionIndex = CharAt(list.ElementAt(i).FullName, position);
                if (charPositionIndex < charAtPosition)
                {
                    list.Swap(lessThan++, i++);
                }
                else if (charPositionIndex > charAtPosition)
                {
                    list.Swap(i, greaterThan--);
                }
                else
                {
                    i++;
                }
            }

            // separate into two buckets (primary sorted above)
            Sort(list, lowerBound, lessThan - 1, position);
            if (charAtPosition >= 0)
            {
                Sort(list, lessThan, greaterThan, position + 1);
            }
            Sort(list, greaterThan + 1, higherBound, position);
        }

        /// <summary>
        /// Word comparison
        /// </summary>
        /// <returns>The less.</returns>
        /// <param name="first">First string</param>
        /// <param name="second">Second string</param>
        /// <param name="position">Position to begin comparison from</param>
        private static Boolean WordComparison(String first, String second, int position)
        {
            for (int i = position; i < Math.Min(first.Length, second.Length); i++)
            {
                return first[i] < second[i];
            }
            return first.Length < second.Length;
        }

        /// <summary>
        /// Insertion sort for smaller arrays as per note on Wikipedia
        /// </summary>
        /// <see cref="https://en.wikipedia.org/wiki/Multi-key_quicksort"/>
        /// <param name="customerList">Customer list.</param>
        /// <param name="lowerBound">Lower bound.</param>
        /// <param name="higherBound">Higher bound.</param>
        /// <param name="position">Position.</param>
        private static void insertion(IList<Customer> customerList, int lowerBound, int higherBound, int position)
        {
            for (int i = lowerBound; i <= higherBound; i++)
            {
                for (int j = i; j > lowerBound && WordComparison(customerList.ElementAt(j).FullName, customerList.ElementAt(j - 1).FullName, position); j--)
                {
                    customerList.Swap(j, j - 1);
                }
                   
            }
        }

    }
}
