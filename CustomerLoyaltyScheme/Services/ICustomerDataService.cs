﻿using CustomerLoyaltyScheme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerLoyaltyScheme.Services
{
    public interface ICustomerDataService
    {
        Customer FindCustomerWithTheHighestLoyaltyPoints(ICollection<Customer> customers);

        ICollection<Customer> FindCustomerWithDuplicateLoyaltyPoints(ICollection<Customer> customers, Customer customer);

        ICollection<Customer> SearchCustomerName(ICollection<Customer> customerList, string name);

        ICollection<Customer> FindDuplicateCustomerNames(ICollection<Customer> customerList);

        ICollection<Customer> SortCustomerByCategory(ICollection<Customer> customers);

        ICollection<Customer> SortCustomerInAlphabeticOrder(ICollection<Customer> customers);

        int GetAverageLoyaltyPoints(ICollection<Customer> customers);
    }
}
