﻿using CustomerLoyaltyScheme.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerLoyaltyScheme.Data
{
    public interface IDataSource
    {
        IList<Customer> GetSeedData(int length = 1000);
    }
}
