﻿using CustomerLoyaltyScheme.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Faker;
using CustomerLoyaltyScheme.Helpers;
using CustomerLoyaltyScheme.Services;

namespace CustomerLoyaltyScheme.Data
{
    public class DataSource: IDataSource
    {
        // Generate Seed data for testing
        public IList<Customer> GetSeedData(int length = 1000)
        {
            // let's not recreate seed data, if set
            if (Helper.CUSTOMER_LIST.Count != 0) {
                return Helper.CUSTOMER_LIST;
            }

            // instantiate an empty List to be populated with dummy customer records 
            var customers = new List<Customer>();
            for (var i=1; i <= length; i++)
            {
                var points = RandomNumber.Next(0, 6000);
                var customer = new Customer(Name.First() + " " + Name.Last(), points, i);
                customers.Add(customer);
            }

            var firstCustomer = customers.First();
            // ensure dupliicate by name exists
            customers.Add(new Customer(firstCustomer.FullName, firstCustomer.LoyaltyPoints + RandomNumber.Next(200, 1000)));
            var sortDriver = new CustomerSort(customers);

            Helper.CUSTOMER_LIST = sortDriver.GetList().ToList();
            return Helper.CUSTOMER_LIST;
        }
    }
}
