﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerLoyaltyScheme.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public int LoyaltyPoints { get; set; }

        // For now, we may as well just have the category by a string.
        public string Category;

        public Customer(string fullName, int loyaltyPoints, int id = 0)
        {
            this.FullName = fullName;
            this.LoyaltyPoints = loyaltyPoints;
            this.Id = id;
            CalculateCategory(loyaltyPoints);
        }

        private void CalculateCategory(int loyaltyPoints)
        {
            if (loyaltyPoints > 0 && loyaltyPoints <= 1000)
            {
                this.Category = "Silver";
            }
            else if (loyaltyPoints > 1000 && loyaltyPoints <= 5000)
            {
                this.Category = "Gold";
            }
            else if (loyaltyPoints > 5000)
            {
                this.Category = "Platinum";
            }
            else
            {
                this.Category = "Undefined";
            }
        }


        public override string ToString()
        {
            return String.Format("{0}, {1}, {2}", this.FullName, this.LoyaltyPoints, this.Category);
        }
    }
}
